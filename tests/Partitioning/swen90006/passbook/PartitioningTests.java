package swen90006.passbook;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;


import static org.junit.Assert.*;

public class PartitioningTests
{
    protected PassBook pb;

    //Any method annotated with "@Before" will be executed before each test,
    //allowing the tester to set up some shared resources.
    @Before public void setUp() throws DuplicateUserException, WeakPassphraseException
    {
	pb = new PassBook();
	pb.addUser("passbookUsername", "Abcd12345");
    }

    //Any method annotated with "@After" will be executed after each test,
    //allowing the tester to release any shared resources used in the setup.
    @After public void tearDown()
    {
    }
    //addUser Test case
    @Test(expected = DuplicateUserException.class) 
    public void addUserEC1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsername", "properPassphrase1");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsername2", "Abbb1");
    }
    @Test
    public void addUserEC3Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsername3", "Aaab13222");
    	assertTrue(pb.isUser("passbookUsername3"));

    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC4Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "123456789");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC5Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "abcdefghijk");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC6Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "ABCDEFGHIJK");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC7Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "ABCD12345");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC8Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "abcd12345");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC9Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "ABCDabcde");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC10Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "-=-@$#@**()&^%");
    }
    @Test
    public void addUserEC11Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsername3", "Aaab1222");
    	assertTrue(pb.isUser("passbookUsername3"));

    }
    
    // loginUser test case    
    @Test(expected = NoSuchUserException.class) 
    public void loginUserEC1Test()throws NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException
    {
    	pb.loginUser("passbookUsernamee", "Abcd12345");
    }
    @Test(expected = AlreadyLoggedInException.class) 
    public void loginUserEC2Test()throws NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException
    {
    	pb.loginUser("passbookUsername", "Abcd12345");
    	pb.loginUser("passbookUsername", "Abcd12345");
    	} 
    @Test(expected = IncorrectPassphraseException.class) 
    public void loginUserEC3Test()throws NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException
    {
    	pb.loginUser("passbookUsername", "ABCDEa12345");
    }  
    @Test
    public void loginUserEC4Test()throws NoSuchUserException, AlreadyLoggedInException, IncorrectPassphraseException
    {
    	int sessionID=pb.loginUser("passbookUsername", "Abcd12345");
    	pb.logoutUser(sessionID);
    	} 
    
    // updateDetails test case
    @Test(expected = InvalidSessionIDException.class) 
    public void updateDetailsEC1Test()throws Throwable
    {
    	URL url = new URL("http://helloworld.com");
    	int sessionID=pb.loginUser("passbookUsername", "Abcd12345");
    	pb.logoutUser(sessionID);
    	pb.updateDetails(sessionID, url, "passbookUsername", "Abcd12345");
    	} 
    @Test(expected = MalformedURLException.class) 
    public void updateDetailsEC2Test()throws Throwable
    {
    	URL url = new URL("://helloworld.com");
    	int sessionID=pb.loginUser("passbookUsername", "Abcd12345");
    	pb.logoutUser(sessionID);
    	pb.updateDetails(sessionID, url, "passbookUsername", "Abcd12345");
    	} 
    @Test
    public void updateDetailsEC3Test()throws Throwable
    {
    	URL url = new URL("https://helloworld.com");
    	int sessionID=pb.loginUser("passbookUsername", "Abcd12345");
    	pb.updateDetails(sessionID, url, "passbookUsername", "Abcd12345");
    	assertNotNull(pb.retrieveDetails(sessionID, url));
    	}     
    @Test (expected = NoSuchURLException.class)
    public void updateDetailsEC4Test()throws Throwable
    {
    	URL url = new URL("http://helloworld.com");
 
    	int sessionID=pb.loginUser("passbookUsername", "Abcd12345");
    	pb.updateDetails(sessionID, url, null, "Abcd12345");
    	pb.retrieveDetails(sessionID, url);
    	} 
    @Test (expected = NoSuchURLException.class)
    public void updateDetailsEC5Test()throws Throwable
    {
    	URL url = new URL("http://helloworld.com");
 
    	int sessionID=pb.loginUser("passbookUsername", "Abcd12345");
    	pb.updateDetails(sessionID, url, "passbookUsername", null);
    	pb.retrieveDetails(sessionID, url);
    	}
    @Test (expected = NoSuchURLException.class)
    public void updateDetailsEC6Test()throws Throwable
    {
    	URL url = new URL("http://helloworld.com");
 
    	int sessionID=pb.loginUser("passbookUsername", "Abcd12345");
    	pb.updateDetails(sessionID, url, null, null);
    	pb.retrieveDetails(sessionID, url);
    	}   
    @Test
    public void updateDetailsEC7Test()throws Throwable
    {
    	URL url = new URL("http://helloworld.com");
 
    	int sessionID=pb.loginUser("passbookUsername", "Abcd12345");
    	pb.updateDetails(sessionID, url, "passbookUsername", "Abcd12345");
    	assertNotNull(pb.retrieveDetails(sessionID, url));
    	}  
    //retrieveDetails test case
    @Test(expected = InvalidSessionIDException.class) 
    public void retrieveDetailsEC1Test()throws Throwable
    {
    	URL url = new URL("http://helloworld.com");
    	int sessionID=pb.loginUser("passbookUsername", "Abcd12345");
    	pb.logoutUser(sessionID);
    	pb.updateDetails(sessionID, url, "passbookUsername", "Abcd12345");
    	assertNotNull(pb.retrieveDetails(sessionID, url));
    	} 
    @Test(expected = MalformedURLException.class) 
    public void retrieveDetailsEC2Test()throws Throwable
    {
    	URL url = new URL("://helloworld.com");
    	int sessionID=pb.loginUser("passbookUsername", "Abcd12345");
    	pb.logoutUser(sessionID);
    	pb.updateDetails(sessionID, url, "passbookUsername", "Abcd12345");
    	pb.retrieveDetails(sessionID, url);
    	} 
    @Test
    public void retrieveDetailsEC3Test()throws Throwable
    {
    	URL url = new URL("https://helloworld.com");
    	int sessionID=pb.loginUser("passbookUsername", "Abcd12345");
    	pb.updateDetails(sessionID, url, "passbookUsername", "Abcd12345");
    	pb.retrieveDetails(sessionID, url);
    	}   
    @Test(expected = NoSuchURLException.class) 
    public void retrieveDetailsEC4Test()throws Throwable
    {
    	URL url = new URL("http://helloworld.com");
    	int sessionID=pb.loginUser("passbookUsername", "Abcd12345");
    	//pb.updateDetails(sessionID, url, "passbookUsername", "Abcd1234");
    	pb.retrieveDetails(sessionID, url);
    	} 
    @Test(expected = NoSuchURLException.class) 
    public void retrieveDetailsEC5Test()throws Throwable
    {
    	URL url1 = new URL("http://helloworld.com");
    	int sessionID=pb.loginUser("passbookUsername", "Abcd12345");
    	pb.updateDetails(sessionID, url1, "passbookUsername", "Abcd12345");
    	URL url2 = new URL("http://helloworldddd.com");

    	pb.retrieveDetails(sessionID, url2);
    	} 
    @Test
    public void retrieveDetailsEC6Test()throws Throwable
    {
    	URL url = new URL("http://helloworld.com");
    	int sessionID=pb.loginUser("passbookUsername", "Abcd12345");
    	pb.updateDetails(sessionID, url, "passbookUsername", "Abcd12345");
    	assertNotNull(pb.retrieveDetails(sessionID, url));
    	}   
}


