package swen90006.passbook;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

//By extending PartitioningTests, we inherit tests from the script
public class BoundaryTests
    extends PartitioningTests
{
    //Add another test
    @Test public void anotherTEst()
    {
	//include a message for better feedback
	final int expected = 2;
	final int actual = 2;
	assertEquals("Some failure message", expected, actual);
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC2Offpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsername2", "Abbb123");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC2Offpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsername2", "");
    }
    @Test
    public void addUserEC3Onpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsername3", "AAaa00000");
    	assertTrue(pb.isUser("passbookUsername3"));

    }
    @Test
    public void addUserEC3Onpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsername3", "ZZzz99999");
    	assertTrue(pb.isUser("passbookUsername3"));

    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC3Offpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "``//////@@");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC3Offpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "{{::::::[[[");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC4Offpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "@@`````0000");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC4Offpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "[[[{{{{{{999999");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC5Offpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "@@/////aaaaa");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC5Offpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "[[[[:::::::zz");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC6Offpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "````///////AA");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC6Offpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "{{{{{::::::ZZ");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC7Offpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "``AAAAA000");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC7Offpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "{{{ZZZZ9999");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC8Offpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "@aaaaa000");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC8Offpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "[[[zzzzz9999");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC9Offpoint1Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "aaa///AAAA");
    }
    @Test(expected = WeakPassphraseException.class) 
    public void addUserEC9Offpoint2Test()throws DuplicateUserException, WeakPassphraseException
    {
    	pb.addUser("passbookUsernamee", "zzz::::::ZZZ");
    }
}
